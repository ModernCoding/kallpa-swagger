'use strict';

var utils = require('../utils/writer.js');
var Enum = require('../service/EnumService');

module.exports.professional_information_enumGET = function professional_information_enumGET (req, res, next) {
  Enum.professional_information_enumGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
