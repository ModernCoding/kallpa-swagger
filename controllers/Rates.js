'use strict';

var utils = require('../utils/writer.js');
var Rates = require('../service/RatesService');

module.exports.ratesGET = function ratesGET (req, res, next) {
  Rates.ratesGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.update_rateGET = function update_rateGET (req, res, next) {
  Rates.update_rateGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
