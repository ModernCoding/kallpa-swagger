'use strict';

var utils = require('../utils/writer.js');
var Causes = require('../service/CausesService');

module.exports.causesGET = function causesGET (req, res, next) {
  Causes.causesGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
