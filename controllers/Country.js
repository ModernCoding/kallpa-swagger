'use strict';

var utils = require('../utils/writer.js');
var Country = require('../service/CountryService');

module.exports.countriesGET = function countriesGET (req, res, next) {
  Country.countriesGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.countriesResidency_countriesGET = function countriesResidency_countriesGET (req, res, next) {
  Country.countriesResidency_countriesGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
