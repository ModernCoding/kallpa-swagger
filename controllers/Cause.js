'use strict';

var utils = require('../utils/writer.js');
var Cause = require('../service/CauseService');

module.exports.causesCauseIdDELETE = function causesCauseIdDELETE (req, res, next, causeId) {
  Cause.causesCauseIdDELETE(causeId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.causesCauseIdGET = function causesCauseIdGET (req, res, next, causeId) {
  Cause.causesCauseIdGET(causeId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.causesCauseIdPOST = function causesCauseIdPOST (req, res, next, causeId) {
  Cause.causesCauseIdPOST(causeId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
