'use strict';

var utils = require('../utils/writer.js');
var InviteToMyCommunity = require('../service/InviteToMyCommunityService');

module.exports.invite_to_my_communityPOST = function invite_to_my_communityPOST (req, res, next, body) {
  InviteToMyCommunity.invite_to_my_communityPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
