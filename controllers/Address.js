'use strict';

var utils = require('../utils/writer.js');
var Address = require('../service/AddressService');

module.exports.addressGET = function addressGET (req, res, next, q) {
  Address.addressGET(q)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.addressPostal_codeGET = function addressPostal_codeGET (req, res, next, q) {
  Address.addressPostal_codeGET(q)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
