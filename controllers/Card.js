'use strict';

var utils = require('../utils/writer.js');
var Card = require('../service/CardService');

module.exports.cardCard_balanceGET = function cardCard_balanceGET (req, res, next) {
  Card.cardCard_balanceGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.cardDELETE = function cardDELETE (req, res, next) {
  Card.cardDELETE()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.cardGET = function cardGET (req, res, next) {
  Card.cardGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.cardPUT = function cardPUT (req, res, next, body) {
  Card.cardPUT(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
