'use strict';

var utils = require('../utils/writer.js');
var SignIn = require('../service/SignInService');

module.exports.sign_inPOST = function sign_inPOST (req, res, next, body) {
  SignIn.sign_inPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
