'use strict';

var utils = require('../utils/writer.js');
var Donate = require('../service/DonateService');

module.exports.donatePOST = function donatePOST (req, res, next, body) {
  Donate.donatePOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
