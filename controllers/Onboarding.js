'use strict';

var utils = require('../utils/writer.js');
var Onboarding = require('../service/OnboardingService');

module.exports.onboardingPOST = function onboardingPOST (req, res, next, body) {
  Onboarding.onboardingPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
