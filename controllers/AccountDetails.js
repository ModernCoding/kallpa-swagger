'use strict';

var utils = require('../utils/writer.js');
var AccountDetails = require('../service/AccountDetailsService');

module.exports.homeGET = function homeGET (req, res, next) {
  AccountDetails.homeGET()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
