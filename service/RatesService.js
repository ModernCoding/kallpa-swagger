'use strict';


/**
 * Get the list of common rates
 * Get the list of common rates
 *
 * returns List
 **/
exports.ratesGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ 0.5, 1, 1.5, 2, 3, 5, 7, 9, 10 ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get the list of common rates and user next month rate
 * Get the list of common rates and user next month rate
 *
 * returns UpdateRate
 **/
exports.update_rateGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "rates" : [ 0.5, 1, 1.5, 2, 3, 5, 7, 9, 10 ],
  "nextMonthUserRate" : 0.005
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

