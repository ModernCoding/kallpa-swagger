'use strict';


/**
 * Remove a cause from the user mix
 * Remove a cause from the user mix
 *
 * causeId String Id of the cause
 * no response value expected for this operation
 **/
exports.causesCauseIdDELETE = function(causeId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get one cause by id
 * Get one cause by id
 *
 * causeId String Id of the cause
 * returns CauseDetails
 **/
exports.causesCauseIdGET = function(causeId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "totalDonors" : 4265,
  "name" : "Médecins du monde",
  "description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vehicula ipsum quis nisl auctor blandit. Etiam nisl mauris, vestibulum in  commodo quis, lobortis sed lorem. Mauris laoreet eget mauris id tincidunt. Quisque dapibus convallis turpis, eget dictum urna lobortis eu.  Aenean convallis consequat tristique. Duis at risus eget erat vulputate molestie. Suspendisse ornare nisl elit, sit amet condimentum tortor  ultrices tincidunt. Sed ac felis quis neque dictum porttitor. In hac habitasse platea dictumst. Donec quis lobortis mauris. Vestibulum rutrum  velit sit amet finibus suscipit. Mauris tincidunt nisi nec convallis vestibulum. Suspendisse mollis enim neque, at hendrerit justo ullamcorper sit amet.",
  "totalDonation" : {
    "currency" : "EUR",
    "value" : 269.2
  },
  "nextMonthMix" : true,
  "id" : "1234",
  "logoUrl" : "https://"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Add the cause to the user mix
 * Add the cause to the user mix
 *
 * causeId String Id of the cause
 * no response value expected for this operation
 **/
exports.causesCauseIdPOST = function(causeId) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

