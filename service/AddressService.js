'use strict';


/**
 * Get a list of address
 * Get a list of address
 *
 * q String the user search
 * returns List
 **/
exports.addressGET = function(q) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "city" : "Paris",
  "postalCode" : "75002",
  "addressLine1" : "1 rue de la paix"
}, {
  "city" : "Paris",
  "postalCode" : "75002",
  "addressLine1" : "1 rue de la paix"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get a list of cities
 * Get a list of cities
 *
 * q String the user search
 * returns List
 **/
exports.addressPostal_codeGET = function(q) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ "Vigneux-de-Breatgne", "Cordemais" ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

