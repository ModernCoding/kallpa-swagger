'use strict';


/**
 * Create a user
 * Create a user
 *
 * body UserInput 
 * returns UserResponse
 **/
exports.userPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "loginInfo" : {
    "loginUrl" : "https://authent.io",
    "stateId" : 12345,
    "mobilePhoneNumber" : "+33655222"
  },
  "user" : {
    "mobilePhoneNumber" : "+336000000000",
    "firstName" : "John",
    "lastName" : "Smith",
    "nationalityCode" : "FRA",
    "birthDate" : "1980-01-15",
    "email" : "johnsmith@gmail.com"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

