'use strict';


/**
 * Get all  the causes for my profile
 * Get all  the causes for my profile
 *
 * returns List
 **/
exports.causesGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "color" : "#FFFFFF",
  "causeDetails" : [ {
    "name" : "Médecins du monde",
    "id" : "12345",
    "logoUrl" : "https://....",
    "description" : "desription",
    "totalDonation" : {
      "value" : 269,
      "currency" : "EUR"
    },
    "totalDonors" : 10,
    "nextMonthMix" : false
  }, {
    "name" : "Médecins du monde",
    "logoUrl" : "https://....",
    "description" : "desription",
    "totalDonation" : {
      "value" : 2697,
      "currency" : "EUR"
    },
    "totalDonors" : 34,
    "nextMonthMix" : false
  } ],
  "label" : "Faim zéro, Mon mix"
}, {
  "color" : "#FFFFFF",
  "causeDetails" : [ {
    "name" : "Médecins du monde",
    "id" : "12345",
    "logoUrl" : "https://....",
    "description" : "desription",
    "totalDonation" : {
      "value" : 269,
      "currency" : "EUR"
    },
    "totalDonors" : 10,
    "nextMonthMix" : false
  }, {
    "name" : "Médecins du monde",
    "logoUrl" : "https://....",
    "description" : "desription",
    "totalDonation" : {
      "value" : 2697,
      "currency" : "EUR"
    },
    "totalDonors" : 34,
    "nextMonthMix" : false
  } ],
  "label" : "Faim zéro, Mon mix"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

