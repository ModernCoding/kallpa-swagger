'use strict';


/**
 * Get the countries list
 * Get the countries list
 *
 * returns List
 **/
exports.countriesGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "code" : "FRA",
  "phoneCode" : "+33",
  "label" : "France",
  "flagUrl" : "https://"
}, {
  "code" : "FRA",
  "phoneCode" : "+33",
  "label" : "France",
  "flagUrl" : "https://"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get the list of countries for the residency address
 * Get the list of countries for the residency address
 *
 * returns List
 **/
exports.countriesResidency_countriesGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "code" : "FRA",
  "phoneCode" : "+33",
  "label" : "France",
  "flagUrl" : "https://"
}, {
  "code" : "FRA",
  "phoneCode" : "+33",
  "label" : "France",
  "flagUrl" : "https://"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

