'use strict';


/**
 * Get the sign-in information
 * Get the sign-in information
 *
 * body SignInInput 
 * returns SignInResponse
 **/
exports.sign_inPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "onboardingUrl" : "https://",
  "stateId" : "12345",
  "mobilePhoneNumber" : 3365555
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

