'use strict';


/**
 * Get the card balance
 * Get the card balance
 *
 * returns CardBalance
 **/
exports.cardCard_balanceGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "cardBalance" : {
    "currency" : "EUR",
    "value" : 269.2
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Cancel a card
 * Cancel a card
 *
 * no response value expected for this operation
 **/
exports.cardDELETE = function() {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get the user cardDetails
 * Get the user cardDetails
 *
 * returns CardDetails
 **/
exports.cardGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "expiryDate" : "12/23",
  "ecommerce" : true,
  "spendingLimit" : {
    "currency" : "EUR",
    "value" : 269.2
  },
  "nonMainCurrencyTransactions" : true,
  "cardMaskedNumber" : "1234 xxx xxxx xxxx",
  "name" : "John G. Smith",
  "withdrawal" : false,
  "international" : true
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update the user card details
 * Update the user card details
 *
 * body CardDetails 
 * returns CardDetails
 **/
exports.cardPUT = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "expiryDate" : "12/23",
  "ecommerce" : true,
  "spendingLimit" : {
    "currency" : "EUR",
    "value" : 269.2
  },
  "nonMainCurrencyTransactions" : true,
  "cardMaskedNumber" : "1234 xxx xxxx xxxx",
  "name" : "John G. Smith",
  "withdrawal" : false,
  "international" : true
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

