'use strict';


/**
 * Get all the information of an account
 * Get all the information of an account
 *
 * returns AccountDetails
 **/
exports.homeGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "userInfo" : {
    "firstName" : "John",
    "lastName" : "G. Smith",
    "rate" : 0.002
  },
  "donation" : {
    "monthDonationTaxDeducted" : {
      "value" : 5.25,
      "currency" : "EUR"
    },
    "monthDonation" : {
      "value" : 15.46,
      "currency" : "EUR"
    }
  },
  "community" : {
    "communityAmount" : {
      "value" : 1200,
      "currency" : "EUR"
    }
  },
  "transactions" : [ {
    "date" : "2023-03-15",
    "transactionAmount" : {
      "value" : -245.47,
      "currency" : "EUR"
    },
    "donationAmount" : {
      "value" : -5.69,
      "currency" : "EUR"
    },
    "label" : "Chez Dame Julie",
    "type" : "carte bancaire",
    "donation" : false
  }, {
    "date" : "2023-03-14",
    "transactionAmount" : {
      "value" : 35,
      "currency" : "EUR"
    },
    "label" : "virement reçu",
    "type" : "Rechargement",
    "donation" : false
  }, {
    "date" : "2023-03-13",
    "transactionAmount" : {
      "value" : -10,
      "currency" : "EUR"
    },
    "label" : "donc à Unicef",
    "type" : "virement",
    "donation" : true
  } ],
  "account" : {
    "pendingBalance" : {
      "currency" : "EUR",
      "value" : 35.5
    },
    "iban" : "FR00000000000",
    "bookedBalance" : {
      "currency" : "EUR",
      "value" : 269.2
    },
    "bic" : "BIC00000"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

