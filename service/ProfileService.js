'use strict';


/**
 * Get the user profile
 * Get the user profile
 *
 * returns ProfileDetails
 **/
exports.profileGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "lastName" : "G. Smith",
  "mobilePhoneNumber" : "+33625378739",
  "country" : "France",
  "city" : "Paris",
  "postalCode" : "75012",
  "employmentStatus" : "employé",
  "birthDate" : "1980-01-01",
  "firstName" : "John",
  "nationality" : "France",
  "nationalityFlag" : "https://",
  "idVerified" : {
    "currency" : "EUR",
    "value" : 269.2
  },
  "userRate" : 0.05,
  "addressLine1" : "1 rue de la paix",
  "addressLine2" : "lieu dit",
  "state" : "",
  "email" : "john@mail.com",
  "monthlyIncome" : "Entre 1500 € et 3000 €"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Update the user profile
 * Update the user profile
 *
 * body ProfileDetails 
 * returns ProfileDetails
 **/
exports.profilePUT = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "lastName" : "G. Smith",
  "mobilePhoneNumber" : "+33625378739",
  "country" : "France",
  "city" : "Paris",
  "postalCode" : "75012",
  "employmentStatus" : "employé",
  "birthDate" : "1980-01-01",
  "firstName" : "John",
  "nationality" : "France",
  "nationalityFlag" : "https://",
  "idVerified" : {
    "currency" : "EUR",
    "value" : 269.2
  },
  "userRate" : 0.05,
  "addressLine1" : "1 rue de la paix",
  "addressLine2" : "lieu dit",
  "state" : "",
  "email" : "john@mail.com",
  "monthlyIncome" : "Entre 1500 € et 3000 €"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

