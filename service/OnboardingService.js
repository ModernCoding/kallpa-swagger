'use strict';


/**
 * Create an onboarding
 * Create an onboarding
 *
 * body Onboarding 
 * returns OnboardingResponse
 **/
exports.onboardingPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "onboardingUrl" : "https://",
  "stateId" : "12345"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

