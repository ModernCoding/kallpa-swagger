'use strict';


/**
 * Get the JWT token
 * Get the JWT token
 *
 * stateId String StateId
 * returns LoginResponse
 **/
exports.loginPOST = function(stateId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "accessToken" : "12354fgtrh",
  "accessTokenValidity" : 12000
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

