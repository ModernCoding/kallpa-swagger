'use strict';


/**
 * Get the professional information enums
 * Get the professional information enums
 *
 * returns ProfessionalInformationEnum
 **/
exports.professional_information_enumGET = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "monthlyIncomeEnum" : [ {
    "code" : "LessThan500",
    "label" : "Moins de 500€"
  }, {
    "code" : "LessThan500",
    "label" : "Moins de 500€"
  } ],
  "employmentStatusEnum" : [ {
    "code" : "Employee",
    "label" : "Employé"
  }, {
    "code" : "Employee",
    "label" : "Employé"
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

