'use strict';


/**
 * Send an invitation to someone to join my community
 * Send an email
 *
 * body EmailRequest 
 * returns Response
 **/
exports.invite_to_my_communityPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "success" : true,
  "message" : "Ce client a déjà un compte, il sera ajouté à votre communauté"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

